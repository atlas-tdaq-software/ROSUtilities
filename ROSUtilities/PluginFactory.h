// -*- c++ -*- $Id$
// //////////////////////////////////////////////////////////////////////
//
//  Author:  G.J.Crone, University College London
//
//  $Log$
//  Revision 1.9  2004/02/23 15:59:34  glehmann
//  added typename where needed
//
//  Revision 1.8  2003/04/22 07:52:54  glehmann
//  prepend std::...
//
//  Revision 1.7  2003/03/10 17:59:07  glehmann
//  bug fix and gcc-3.2 compatibility
//
//  Revision 1.6  2003/03/04 19:28:53  gcrone
//  Try to load library without "ROS" prefix before trying with.
//
//  Revision 1.5  2003/02/21 16:20:37  gcrone
//  Changed optional config to void*
//
//  Revision 1.4  2003/02/21 15:04:08  gcrone
//  Call create function with Config* argument if given.
//
//  Revision 1.3  2003/01/30 08:37:36  gcrone
//  Add RTLD_GLOBAL to dlopen flags.
//
//  Revision 1.2  2002/11/07 19:16:16  gcrone
//  New exception handling.
//
//  Revision 1.1.1.1  2002/10/07 09:50:07  pyannis
//  Initial version of DataFlow ROS packages tree...
//
//  Revision 1.2  2002/09/17 21:45:56  gcrone
//  Document code only - No functional changes
//
//  Revision 1.1.1.1  2002/08/23 13:36:55  gorini
//  First import
//
//  Revision 1.1  2002/08/01 14:15:52  gcrone
//  First 'working' version using ROS namespace and dynamically loaded libraries
//
//
// //////////////////////////////////////////////////////////////////////


#ifndef PLUGINFACTORY_H
#define PLUGINFACTORY_H
#include <dlfcn.h>
#include <string>
#include <map>

#include "ROSUtilities/UtilitiesException.h"

/**
@brief  Template class to load shared libraries containing derived classes
 of given name and later call constructor.

  Each shared library implementation must include a function (with C
 rather than C++ linkage) called createXXX (where XXX is the name of
 the derived class) which calls the constructor of the derived class
 and returns a pointer to the interface class. For example if there
 were an implemenation of the FragmentManager for the UKROBIN
 libFragmentManagerUKRobin.so would contain the compiled version of:

 <PRE>
 extern "C" {<BR>
   extern FragmentManager* createFragmentManagerUKRobin();<BR>
 }<BR>
 FragmentManager* createFragmentManagerUKRobin()<BR>
 {<BR>
   return (new FragmentManagerUKRobin());<BR>
 }
 </PRE>

  Shared libraries are loaded by the load(string name) method which
 registers the name for later use in the create(string name) method
 which will instantiate an object of the required type
*/

//class Config;


template <class Type>
class PluginFactory {
   typedef Type* (*Create)();
   typedef Type* (*CreateWithConfig)(void*);
private:
  class SharedLibrary {
  public:
    SharedLibrary(void* handle, unsigned long create) :
      m_handle(handle), m_create(create) {};
    void *handle() {return m_handle;}
    unsigned long create() {return m_create;}
  private:
    void* m_handle;
    unsigned long m_create;
  };
  static std::map<std::string,SharedLibrary*> m_constructorMap;
public:
   /**  Load the shared library and locate the create function.
        Store a pointer to the create function in our map indexed by
        the library name.
        @param name Name of the class to be loaded (library name excluding
        lib prefix and .so suffix).
   */
   void load(std::string name);

   /** Find the create function for plugin \a name, call it and return
       a pointer to the object it created.
       @param name Name of the class to be instantiated.
   */
   Type* create(std::string name, void* configuration=0);

   /** Unload the dynamic library containing class name and remove it
    from our map.  */
   void unload(std::string name);
};

template <class Type> 
std::map< std::string, typename PluginFactory<Type>::SharedLibrary * > PluginFactory<Type>::m_constructorMap;


template <class Type>
void PluginFactory<Type>::load(std::string name)
{
   if (m_constructorMap.find(name) == m_constructorMap.end()) {
     std::cout << "PluginFactory: loading " << name << std::endl;
     std::string library="lib" + name + ".so";
     void*  dlHandle=dlopen(library.c_str(), RTLD_LAZY|RTLD_GLOBAL);
     char* error=dlerror();
     std::string errorText;
     if (error != 0) {
       errorText=error;
     }

     if (dlHandle == NULL) {
       std::cerr << "Warning could not load " << library;
       library="libROS" + name + ".so";
       std::cerr << ", trying " << library << std::endl;
       dlHandle=dlopen(library.c_str(), RTLD_LAZY|RTLD_GLOBAL);
       error=dlerror();
       if (error != 0) {
	 errorText+=error;
       }
     }
     if (dlHandle == NULL) {
       throw UtilitiesException(UtilitiesException::DLOPEN_ERROR, errorText);
     }
     std::string constructorName="create" + name;
     unsigned long create=(unsigned long) dlsym (dlHandle, constructorName.c_str());
     error=dlerror();
     if (error != NULL)  {
       throw UtilitiesException(UtilitiesException::DLSYM_ERROR, error);
     }
     m_constructorMap[name]=new SharedLibrary (dlHandle, create);
   }
   else {
     std::cout << "PluginFactory: " << name << " is already known!" 
	       << std::endl;
   }
}

template <class Type>
void PluginFactory<Type>::unload(std::string name)
{
   if (m_constructorMap.find(name) != m_constructorMap.end()) {
      dlclose(m_constructorMap[name]->handle());
      delete m_constructorMap[name];
      m_constructorMap.erase(name);
   }
   else {
      throw UtilitiesException(UtilitiesException::NOT_LOADED_ERROR,
                               "Cannot unload " + name);
   }
}

template <class Type>
Type*  PluginFactory<Type>::create(std::string name, void* configuration)
{
   if (m_constructorMap.find(name) != m_constructorMap.end()) {
      if (configuration == 0) {
         Create create = (Create)m_constructorMap[name]->create();
         return (create());
      }
      else {
         CreateWithConfig create =
            (CreateWithConfig)m_constructorMap[name]->create();
	 return (create(configuration));
      }
   }
   else {
      throw UtilitiesException(UtilitiesException::NOT_LOADED_ERROR,
                               "Cannot create " + name);
   }
}
#endif
