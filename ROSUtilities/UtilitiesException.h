// -*- c++ -*-
// $Id$ 

#ifndef UTILITIESEXCEPTION_H
#define UTILITIESEXCEPTION_H

#include "DFExceptions/ROSException.h"
#include <string>

class UtilitiesException : public ROSException {

public:
  enum ErrorCode {DLOPEN_ERROR, DLSYM_ERROR, NOT_LOADED_ERROR} ;   

  UtilitiesException(ErrorCode error) ;
  UtilitiesException(ErrorCode error, std::string description) ;
  UtilitiesException(ErrorCode error, const ers::Context& context) ;
  UtilitiesException(ErrorCode error, std::string description, const ers::Context& context) ;
  UtilitiesException(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context) ;
protected:
   virtual std::string getErrorString(unsigned int errorId) const;
   virtual ers::Issue * clone() const { return new UtilitiesException( *this ); }
   static const char * get_uid() { return "ROS::UtilitiesException"; }
   virtual const char * get_class_name() const { return get_uid(); }
};

inline
UtilitiesException::UtilitiesException(UtilitiesException::ErrorCode error) 
   : ROSException("UtilitiesPackage",error,getErrorString(error)) { }

inline
UtilitiesException::UtilitiesException(UtilitiesException::ErrorCode error,
		std::string description) 
   : ROSException("UtilitiesPackage",error,getErrorString(error),description) { }
inline
UtilitiesException::UtilitiesException(UtilitiesException::ErrorCode error, const ers::Context& context) 
   : ROSException("UtilitiesPackage",error,getErrorString(error),context) { }

inline
UtilitiesException::UtilitiesException(UtilitiesException::ErrorCode error,
		std::string description, const ers::Context& context) 
   : ROSException("UtilitiesPackage",error,getErrorString(error),description, context) { }

inline 
UtilitiesException::UtilitiesException(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context) 
     : ROSException(cause, "UtilitiesPackage", error, getErrorString(error), description, context) {}

inline std::string UtilitiesException::getErrorString(unsigned int errorId) const
{
	std::string result;    
   switch (errorId) {  
   case DLOPEN_ERROR:
      result = "Error from dlopen: " ;
      break;
   case DLSYM_ERROR:
      result = "Error from dlsym" ;
      break;
   case NOT_LOADED_ERROR:
      result = "Error, library not registered";
      break;
   default:
      result = "Unspecified error";
      break;
   }
   return(result);
}
#endif
