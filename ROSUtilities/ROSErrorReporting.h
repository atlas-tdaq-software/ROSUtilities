// -*- c++ -*-
// $Id$ 
//
//  Macros to simplify usage of ERS in the ROS
//
// $Log$
// Revision 1.3  2007/04/23 08:45:34  gcrone
// Added SEND_ROS_INFO macro
//
// Revision 1.2  2006/11/01 10:58:14  gcrone
// Forgot to prepend exceptionClass to errorCode in ENCAPSULATE_ROS_EXCEPTION!!
//
// Revision 1.1  2006/10/31 15:45:08  gcrone
// New file with error reporting macros
//
//
#ifndef ROSERRORREPORTING_H
#define ROSERRORREPORTING_H

#include "ers/ers.h"
#include "ers/Issue.h"


ERS_DECLARE_ISSUE(ROS, MessageIssue, ERS_EMPTY, ERS_EMPTY)


/** \def CREATE_ROS_MESSAGE(instanceName, messageContent) Macro to
    create an object which can bet output to ers through one of the
    ers::info() ers::error() or ers::fatal() methods.

   The messageContent is fed into a stringstream, the output of which
   is used to construct the ers::Issue.  This allows usage of normal
   stream syntax (with <<) to get around the fact that ers streams are
   not streams in the sense of iostream.

*/
#define CREATE_ROS_MESSAGE(instanceName, messageContent) \
    std::ostringstream instanceName##_tStream; \
    instanceName##_tStream << messageContent; \
    ROS::MessageIssue instanceName(ERS_HERE, instanceName##_tStream.str());


/** \def SEND_ROS_INFO(messageContent) Macro to
    create an object and output it to ers through
    the ers::info() method.

   The messageContent is fed into a stringstream, the output of which
   is used to construct the ers::Issue.  This allows usage of normal
   stream syntax (with <<) to get around the fact that ers streams are
   not streams in the sense of iostream.

*/
#define SEND_ROS_INFO(messageContent) \
{ \
    std::ostringstream tStream; \
    tStream << messageContent; \
    ROS::MessageIssue msg(ERS_HERE, tStream.str()); \
    ers::info(msg); \
}


/** \def CREATE_ROS_EXCEPTION(instanceName, exceptionClass, errorCode, messageContent)
   Macro to create a ROSException object.

   \param instanceName name of the variable hold the instantiated
   exception object

   \param exceptionClass  type of the exception to be instantiated

   \param errorCode   enumerated error code from the exceptionClass

   \param messageContent extra information to be streamed into the
   exception description
*/
#define CREATE_ROS_EXCEPTION(instanceName, exceptionClass, errorCode, messageContent) \
    std::ostringstream instanceName##_tStream; \
    instanceName##_tStream << messageContent; \
    exceptionClass instanceName(exceptionClass::errorCode, instanceName##_tStream.str(), ERS_HERE);


/** \def ENCAPSULATE_ROS_EXCEPTION(instanceName, exceptionClass, errorCode, oldException, messageContent)
    Macro to encapsulate an exception inside a ROSException along with
    some explanatory text.
*/
#define ENCAPSULATE_ROS_EXCEPTION(instanceName, exceptionClass, errorCode, oldException, messageContent) \
    std::ostringstream instanceName##_tStream; \
    instanceName##_tStream << messageContent; \
    exceptionClass instanceName(oldException, exceptionClass::errorCode, instanceName##_tStream.str(), ERS_HERE)

#endif
